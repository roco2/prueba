﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Prueba.Api.Models;
using Prueba.Api.Resources;
using Prueba.Core.DTOs;
using Prueba.Core.Models;

namespace Prueba.Api.Controllers
{
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IMapper _mapper;
        private readonly  JwtSettings _jwtSettings;

        public AuthController(
        IMapper mapper, 
        UserManager<User> userManager, 
        RoleManager<Role> roleManager,
        IOptionsSnapshot<JwtSettings> jwtSettings)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtSettings = jwtSettings.Value;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost("register")]
        public async Task<IActionResult> SignUp([FromBody]UserRegisterDto userregister)
        {
            var user = _mapper.Map<UserRegisterDto, User>(userregister);

            var userCreateResult = await _userManager.CreateAsync(user, userregister.Password);

            if (userCreateResult.Succeeded)
            {
                return Ok(new {
                    status = true,
                    message = "User created"
                });
            }

            return BadRequest(new {
                status = false,
                message = userCreateResult.Errors.First().Description
            });
        }

        [HttpPost("login")]
        public async Task<IActionResult> SignIn([FromBody]UserLoginDto userLogin)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userLogin.Email);
            
            if (user is null)
            {
                return NotFound("User not found");
            }

            var logIn = await _userManager.CheckPasswordAsync(user, userLogin.Password);

            if (logIn)
            {
                var rol = await _userManager.GetRolesAsync(user);

                var token = new JwtService(_jwtSettings).GenerateJwt(user, rol);

                return Ok(
                    new{
                    status = true,
                    message = "User Logged", 
                    token = token
                });
            }

            return BadRequest("Email or password incorrect.");
        }


    }
}
