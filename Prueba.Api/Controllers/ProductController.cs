using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Prueba.Core.DTOs;
using Prueba.Core.Interfaces.Repositories;
using Prueba.Core.Models;


namespace Prueba.Api.Controllers
{
    //[Authorize(Roles = "Administrator, Test")]
    [Route("api/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductRepository _iProductRepository;
        private readonly IMapper _mapper;
        public ProductController(IProductRepository iProductRepository, IMapper mapper)
        {
            _iProductRepository = iProductRepository;
            _mapper = mapper;
        }

        // GET api/product
        [HttpGet("")]
        public async Task<IActionResult> Get()
        {
            var products = await _iProductRepository.Get();
            var response = _mapper.Map<List<Product>, List<ProductDto>>(products);
            return Ok(response);
        }

        // GET api/product/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var product = await _iProductRepository.GetbyId(id);
            var response = _mapper.Map<Product, ProductDto>(product);
            return Ok(response);
        }

        // POST api/product
        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] ProductDto product)
        {
            if ((string.IsNullOrEmpty(product.Name)) || 
            (string.IsNullOrEmpty(product.Description))|| 
            (string.IsNullOrEmpty(product.Price)))
            {
                return BadRequest(
                new{ 
                    status = false,
                    message = "Object not be should nullables"
                }
            );
            }
                  

            var response =  await _iProductRepository.Create(product);

            if (response)
            {
                return Ok(
                new{ 
                    status = response,
                    message = "Product created"
                }
            );
            }

            return BadRequest(
                new{ 
                    status = false,
                    message = "Failed"
                }
            );
        }

        // PUT api/product/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ProductDto product)
        {
            if ((string.IsNullOrEmpty(product.Name)) || 
            (string.IsNullOrEmpty(product.Description))|| 
            (string.IsNullOrEmpty(product.Price)))
            {
                return BadRequest(
                new{ 
                    status = false,
                    message = "Object not be should nullables"
                }
            );
            }
            if (id == 0)
            {
                return BadRequest(
                new{ 
                    status = false,
                    message = "Invalid Id"
                });
            }    
                  

            var response =  await _iProductRepository.Update(id, product);

            if (response)
            {
                return Ok(
                new{ 
                    status = response,
                    message = "Product Modified"
                }
            );
            }

            return BadRequest(
                new{ 
                    status = false,
                    message = "Failed"
                }
            );
        }

        // DELETE api/product/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest(
                new{ 
                    status = false,
                    message = "Invalid Id"
                });
            }    
                  

            var response =  await _iProductRepository.Delete(id);

            if (response)
            {
                return Ok(
                new{ 
                    status = response,
                    message = "Product Deleted"
                }
            );
            }

            return BadRequest(
                new{ 
                    status = false,
                    message = "Failed"
                }
            );
        }
    }
}