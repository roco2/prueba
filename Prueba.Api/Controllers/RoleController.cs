using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Prueba.Core.DTOs;
using Prueba.Core.Models;

namespace Prueba.Api.Controllers
{
    [Authorize(Roles = "Administrator")]
    [Route("api/role")]
    [ApiController]
    public class RoleController : ControllerBase
    {        
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IMapper _mapper;

        public RoleController(
        IMapper mapper, 
        UserManager<User> userManager, 
        RoleManager<Role> roleManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        
        [HttpPost("create")]
        public async Task<IActionResult> CreateRole([FromBody]RoleUserDto roleUser)
        {
            if(string.IsNullOrWhiteSpace(roleUser.Role))
            {
                return BadRequest("Role name/email is required");
            }

            var role = new Role
            {
                Name = roleUser.Role
            };

            var roleResponse = await _roleManager.CreateAsync(role);

            if (roleResponse.Succeeded)
            {
                return Ok(new{
                    status = true,
                    message = "Role Created" 
                });
            }

            return BadRequest(new {
                status = false,
                message = roleResponse.Errors.First().Description
            });

        }

        [HttpPost("add")]
        public async Task<IActionResult> AddUserToRole([FromBody]RoleUserDto roleUser)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == roleUser.Email);

            var result = await _userManager.AddToRoleAsync(user, roleUser.Role);

            if (result.Succeeded)
            {
                return Ok(new{
                    status = true,
                    message = $"Role {roleUser.Role} to User {roleUser.Email}" 
                });
            }

            return BadRequest(new {
                status = false,
                message = result.Errors.First().Description
            });
        }

    }
}