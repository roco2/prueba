using AutoMapper;
using Prueba.Core.DTOs;
using Prueba.Core.Models;

namespace Prueba.Api.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<UserRegisterDto, User>()
            .ForMember(u => u.UserName, opt => opt.MapFrom(ur => ur.Email));
            
            CreateMap<ProductDto, Product>();

            //

            CreateMap<Product, ProductDto>();
            

            
        }
    }
}