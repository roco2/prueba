namespace Prueba.Api.Models
{
    public class JwtSettings
    {
        public string Issuer { get; set; }

        public string Secret { get; set; }

        public int Expiration { get; set; }
    }
}