using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Prueba.Data;
using Microsoft.EntityFrameworkCore;
using Prueba.Core.Models;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using Prueba.Api.Models;
using Prueba.Api.Extensions;
using Prueba.Data.Repositories;
using Prueba.Core.Interfaces.Repositories;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;

namespace Prueba.Api
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            //
            services.AddCors(options =>
            {
                
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });

            });
            //
            //Context DB
            services.AddDbContext<PruebaDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("Prueba"))
            );
            //

            //Auth
            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<PruebaDbContext>()
                .AddDefaultTokenProviders();
            
            services.Configure<JwtSettings>(Configuration.GetSection("JwtSettings"));
            var jwtSettings = Configuration.GetSection("JwtSettings").Get<JwtSettings>();
            services.AddAuth(jwtSettings);
            //
            
            //Services
            services.AddTransient<IProductRepository, ProductRepository>();
            //

            //
            var security =
                new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            },
                            UnresolvedReference = true
                        },
                        new List<string>()
                    }
                };
            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer",new OpenApiSecurityScheme
                {
                    Description = "Claims: userId, Rolename, Username",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                });
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Prueba", Version = "v1" });
                options.AddSecurityRequirement(security);
            });

            services.AddSwaggerGenNewtonsoftSupport();
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }
            if (!env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseAuth();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Prueba");
            });
        }
    }
}
