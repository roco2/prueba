namespace Prueba.Core.DTOs
{
    public class RoleUserDto
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}