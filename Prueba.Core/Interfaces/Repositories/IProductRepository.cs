using System.Collections.Generic;
using System.Threading.Tasks;
using Prueba.Core.DTOs;
using Prueba.Core.Models;

namespace Prueba.Core.Interfaces.Repositories
{
    public interface IProductRepository
    {
        Task<bool> Create(ProductDto product);
        Task<List<Product>> Get();
        Task<Product> GetbyId(int id);
        Task<bool> Update(int id, ProductDto product);
        Task<bool> Delete(int id);
    }
}