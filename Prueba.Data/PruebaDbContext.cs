﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Prueba.Core.Models;
using Prueba.Data.Configurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prueba.Data
{
    public class PruebaDbContext: IdentityDbContext<User, Role, Guid>
    {
        public DbSet<Product> Products { get; set; }

        public PruebaDbContext(DbContextOptions<PruebaDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder
               .ApplyConfiguration(new ProductConfiguration());

        }
    }
}
