using System;
using Prueba.Core.Interfaces.Repositories;
using Prueba.Core.Models;
using Prueba.Core.DTOs;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Prueba.Data.Repositories
{
    public class ProductRepository: IProductRepository
    {
        private PruebaDbContext _ctx;

        public ProductRepository(PruebaDbContext ctx)
        {
            _ctx = ctx;
        }

        public async Task<bool> Create(ProductDto product)
        {
            try
            {
                var sql = $@"SELECT insertP('{product.Name}', '{product.Description}','{product.Price}')";
                var p = await _ctx.Database.ExecuteSqlRawAsync(sql);
                return true;
            }
            catch (Exception)
            {
                
                return false;
            }
        }

        public async Task<List<Product>> Get()
        {
            var products = await _ctx.Products.FromSqlRaw("Select * from getall();").ToListAsync();
            return products;
        }

        public async Task<Product> GetbyId(int id)
        {
            var product = await _ctx.Products.FromSqlRaw($"Select * from getbyid({id})").SingleOrDefaultAsync();
            return product;
        }

        public async Task<bool> Update(int id, ProductDto product)
        { 
            try
            {
                var sql = $@"SELECT updateP({id},'{product.Name}', '{product.Description}','{product.Price}')";
                var p = await _ctx.Database.ExecuteSqlRawAsync(sql);
                return true;
            }
            catch (Exception)
            {
                
                return false;
            }
        }

        //Delete Product will be enable or disable
        public async Task<bool> Delete(int id)
        { 
            try
            {
                var sql = $@"SELECT deleteP({id})";
                var p = await _ctx.Database.ExecuteSqlRawAsync(sql);
                return true;
            }
            catch (Exception)
            {
                
                return false;
            }
        }
        
    }
}