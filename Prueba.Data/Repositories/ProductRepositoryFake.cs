using System;
using Prueba.Core.Interfaces.Repositories;
using Prueba.Core.Models;
using Prueba.Core.DTOs;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Prueba.Data.Repositories
{
    public class ProductRepositoryFake: IProductRepository
    {
        private readonly List<Product> _products;

        public ProductRepositoryFake()
        {
            _products = new List<Product>(){
                new Product(){
                    Id = 1,
                    Name = "PC",
                    Description = "PC",
                    Price = "260",
                    IsActive = true
                },
                new Product(){
                    Id = 2,
                    Name = "Battery",
                    Description = "Battery",
                    Price = "490",
                    IsActive = true
                },
                new Product(){
                    Id = 3,
                    Name = "TV",
                    Description = "TV",
                    Price = "450",
                    IsActive = true
                }
            };
        }

        public async Task<bool> Create(ProductDto product)
        {
            try
            {
                var p = new Product(){
                    Id=4,
                    Name = product.Name,
                    Description = product.Description,
                    Price = product.Price,
                    IsActive = true
                };
                _products.Add(p);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<List<Product>> Get()
        {
            return _products;
        }

        public async Task<Product> GetbyId(int id)
        {
            return _products.Where(p=> p.Id == id).SingleOrDefault();
        }

        public async Task<bool> Update(int id, ProductDto product)
        { 
            try
            {
                var pro = _products.SingleOrDefault(p=> p.Id == id);

                pro.Name = product.Name;
                pro.Description = product.Description;
                pro.Price = product.Price;

                return true;
            }
            catch (Exception)
            {
                
                return false;
            }
        }

        //Delete Product will be enable or disable
        public async Task<bool> Delete(int id)
        { 
            try
            {
                var product =  _products.Where(p=> p.Id == id).SingleOrDefault();
                _products.Remove(product);
                return true;
            }
            catch (Exception)
            {
                
                return false;
            }
        }
        
    }
}