﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Prueba.Api.Controllers;
using Prueba.Core.DTOs;
using Prueba.Core.Interfaces.Repositories;
using Prueba.Core.Models;
using Prueba.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Prueba.Test
{
    public class ProductControllerTest
    {
        IProductRepository _productRepositoryFake;
        ProductController _productController;
        IMapper _mapper;

        public ProductControllerTest() 
        {
            _productRepositoryFake = new ProductRepositoryFake();
            _productController = new ProductController(_productRepositoryFake, _mapper);
        }


        [Fact]
        public void Get()
        {
            var products = _productController.Get().Result ;

            // Assert
            Assert.IsType<OkObjectResult>(products);
           
        }

        [Fact]
        public void GetbyId()
        {
            var products = _productController.GetById(1).Result;

            // Assert
            Assert.IsType<OkObjectResult>(products);

        }

        [Fact]
        public void Add()
        {
            var product = new ProductDto() 
            { 
                Name = "Product",
                Description = "Any",
                Price = "100"
            };
            var response = _productController.Post(product).Result;

            // Assert
            Assert.IsType<OkObjectResult>(response);

        }

        [Fact]
        public void Delete()
        {
            var response = _productController.Delete(1).Result;

            // Assert
            Assert.IsType<OkObjectResult>(response);

        }

        [Fact]
        public void Update()
        {
            var product = new ProductDto()
            {
                Name = "PC",
                Description = "PC",
                Price = "260",
            };
            var response = _productController.Put(1, product).Result;

            // Assert
            Assert.IsType<OkObjectResult>(response);

        }
    }
}
