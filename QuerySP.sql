/*

--All Products

CREATE OR REPLACE FUNCTION getall()
 RETURNS SETOF "Products"
 LANGUAGE sql
AS $function$
    SELECT *
    FROM "public"."Products"
    WHERE "IsActive" = true
    ORDER BY "Id", "Name";
$function$

--Select * from getall();

-- Products by id

CREATE OR REPLACE FUNCTION getbyid(idparam integer)
 RETURNS SETOF "Products"
 LANGUAGE sql
AS $function$
    SELECT *
    FROM "public"."Products"
    WHERE "Products"."Id" = idparam
    AND "IsActive" = true;
$function$

-- Select * from getbyid(2);


-- Insert Product

CREATE OR REPLACE FUNCTION insertP(n text, d text, p text) 
RETURNS bool AS $$
DECLARE
    i integer := 0;
BEGIN
    SELECT MAX("Products"."Id")+1 FROM "public"."Products" INTO i;
    INSERT INTO "public"."Products" 
    VALUES (i, n, d, p, true);
    RETURN FOUND;
END
$$
LANGUAGE 'plpgsql';

----SELECT insertP('Display','Monitor Display','200');


-- Update Product

CREATE OR REPLACE FUNCTION updateP(i int, n text, d text, p text) 
RETURNS bool AS $$
BEGIN
    IF  (i isnull OR i = 0) OR 
        (n isnull OR n = '') OR 
        (d isnull OR n = '') OR 
        (p isnull OR p = '') THEN
        RETURN false;
    END IF;
    
    UPDATE "public"."Products" 
    SET 
    "Name" = n,
    "Description" = d,
    "Price" = p
    WHERE "Products"."Id" = i
    ;
    
    RETURN FOUND;
END
$$
LANGUAGE 'plpgsql';

--SELECT updateP(2,'Display','Monitor OTRO UPDATE','344');


--Delete Product

CREATE OR REPLACE FUNCTION deleteP(i int) 
RETURNS bool AS $$
BEGIN
    IF  (i isnull OR i = 0) THEN
        RETURN false;
    END IF;
    
    UPDATE "public"."Products" 
    SET 
    "IsActive" = false
    WHERE "Products"."Id" = i
    ;
    
    RETURN FOUND;
END
$$
LANGUAGE 'plpgsql';

-- SELECT deleteP(3, false);

*/